HUSSON Odran

---

# REST API PHP

---

## Installation

 - Installer la base de donnée dans database.sql
 - Activer le allow overide dans le virtual host pour charger le htaccess
 - Modifier la configuration SQL dans index.php

## Utilisation

### Afficher tous les clients

Appel :
'''URL
http://api.localhost/api/clients
'''

Retour :
'''JSON
[
    {
        "idClient": "1",
        "0": "1",
        "nom": "Francis",
        "1": "Francis",
        "prenom": "Marcel",
        "2": "Marcel",
        "mobile": "0102030405",
        "3": "0102030405"
    },
    {
        "idClient": "2",
        "0": "2",
        "nom": "Georges",
        "1": "Georges",
        "prenom": "Pasteque",
        "2": "Pasteque",
        "mobile": "0610101010",
        "3": "0610101010"
    }
]
'''

### Afficher un client par ID

Appel :
'''URL
http://api.localhost/api/client/{ID}
'''

Retour :
'''JSON
{
    "idClient": "1",
    "0": "1",
    "nom": "Francis",
    "1": "Francis",
    "prenom": "Marcel",
    "2": "Marcel",
    "mobile": "0102030405",
    "3": "0102030405"
}
'''

### Ajouter un client

Appel :
'''URL
http://api.localhost/api/add_client/{NOM}/{PRENOM}/{MOBILE}
'''

Retour :
'''JSON

{
    "result": "Ok",
    "nom": "Jean Francois",
    "prenom": "DELAFORET",
    "mobile": "0612121212"
}
'''

### Supprimer un client

Appel :
'''URL
http://api.localhost/api/delete/{ID}
'''

Retour :
'''JSON
{
    "Result": "ok"
}
'''

On peut vérifier en faisant un appel pour voir les infos du client 4 :
'''JSON
{
    "Error": "Aucun client trouve pour l'id : 4"
}
'''
