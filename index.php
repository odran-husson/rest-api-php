<?php
/*
  Odran HUSSON
  to : alexandre.db@gmail.com
*/
define('MYSQL_HOST', 'localhost');
define('MYSQL_USER', 'debug');
define('MYSQL_PASSWORD', 'debug');
define('MYSQL_DATABASE', 'debug');

$client_api = new client_api;

$client_api->process();

class client_api {
  private $db;

  public function process() {
    if(isset($_GET['api'])) {
      //Récupération de l'appel suppression du potentiel / final
      $request = rtrim($_SERVER['REQUEST_URI'], '/');
      //Suppression d'une chaine inutile
      $request = str_replace('/api/','',$request);
      //On sépare les éléments de l'appel
      $request_parts = explode('/', $request);

      //HEADER FOR PROPPER RETURN
      header('Content-Type: application/json');
      //Le premier élément correspond au type d'action demandé
      $actions = $request_parts[0];
      switch ($actions) {
        case 'clients':
            echo $this->get_all_clients();
          break;
        case 'client':
          if(isset($request_parts[1]) && is_numeric($request_parts[1])) {
            echo $this->get_client_by_id($request_parts[1]);
          }
          else {
            echo $this->error('Identifiant non valide');
          }
          break;
        case 'add_client':
          if(count($request_parts) != 4) {
            echo $this->error('Error wrong argument number please see the documentation');
          }
          else {
            $this->add_client($request_parts[1],$request_parts[2],$request_parts[3]);
          }
        case 'delete':
          if(isset($request_parts[1]) && is_numeric($request_parts[1])) {
            echo $this->delete_client($request_parts[1]);
          }
          else {
            echo $this->error('Identifiant non valide');
          }
          break;
        default:
          echo $this->error('Error wrong argument number please see the documentation');
          break;
      }
    }
    else {
      echo $this->error('Error wrong argument number please see the documentation');
    }
  }

  private function db_connect() {
    try {
      $this->db = new PDO('mysql:host='.MYSQL_HOST.';dbname='.MYSQL_DATABASE, MYSQL_USER, MYSQL_PASSWORD);
    }
    catch(Exception $e) {
      echo $this->error($e->getMessage());
      die();
    }
  }

  private function get_all_clients()
  {
      $this->db_connect();
      $req = $this->db->query('SELECT * FROM client');

      $return = array();

      while ($data = $req->fetch()) {
          $return[] = $data;
      }

      return json_encode($return, JSON_PRETTY_PRINT);
  }

  private function get_client_by_id($id)
  {
      $this->db_connect();
      $req = $this->db->prepare('SELECT * FROM client WHERE idClient = :id LIMIT 0,1');
      $req->execute(array(
          "id" => $id
      ));
      $data = $req->fetch();

      if($data == false) {
        return $this->error('Aucun client trouve pour l\'id : '.$id);
      }
      else {
        return json_encode($data, JSON_PRETTY_PRINT);
      }
  }
  private function add_client($nom,$prenom,$mobile) {
    try {
      $this->db_connect();
      $req = $this->db->prepare('INSERT INTO client (nom,prenom,mobile) VALUES (:nom,:prenom,:mobile)');
      $call = array(
        "nom" => urldecode($nom),
        "prenom" => urldecode($prenom),
        "mobile" => str_replace(' ','',urldecode($mobile))
      );
      $req->execute($call);

      $result = array(
        "result" => 'Ok',
        "nom" => urldecode($nom),
        "prenom" => urldecode($prenom),
        "mobile" => str_replace(' ','',urldecode($mobile))
      );
      echo json_encode($result,JSON_PRETTY_PRINT);
    }
    catch(Exception $e) {
      echo $this->error($e->getMessage());
    }
  }

  private function delete_client($id) {
    try {
      $this->db_connect();
      $req = $this->db->prepare('DELETE FROM client WHERE idClient = :id');
      $req->execute(array(
        "id" => $id
      ));
      echo json_encode(array('Result' => "ok"),JSON_PRETTY_PRINT);
    }
    catch(Exception $e) {
      echo $this->error($e->getMessage());
    }

  }

  private function error($message) {
    return json_encode(array('Error' => $message),JSON_PRETTY_PRINT);
  }
}
?>
